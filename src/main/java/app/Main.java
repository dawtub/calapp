package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.DriverManager;

public class Main extends Application {

    private static final String dbUrl = "jdbc:mysql://127.0.0.1:3306/produkty";
    private static final String dbLogin = "root";
    private static final String dbPassword = "";
    public static  Connection dbCon;

    static {
        try {
            dbCon = DriverManager.getConnection(dbUrl, dbLogin, dbPassword);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/views/Wejsciowa.fxml"));

        Scene scene = new Scene(root);
        stage.getIcons().add(new Image("/icon.jpg"));
        stage.setTitle("Licznik kalorii");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
