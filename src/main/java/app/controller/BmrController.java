package app.controller;

import app.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;

public class BmrController extends TabsController {

    @FXML
    private javafx.scene.control.TextField height;
    @FXML
    private javafx.scene.control.TextField age;
    @FXML
    private javafx.scene.control.TextField weight;
    @FXML
    private javafx.scene.control.TextField result;

    private Connection con = Main.dbCon;


    public void caloriesOnReduction(ActionEvent actionEvent) {
        float result = calulateBMR() - 300;
        insertBMR(result);
    }

    public void caloriesOnBulking(ActionEvent actionEvent) {
        float result = calulateBMR() + 300;
        insertBMR(result);
    }

    public void caloriesOnMaintaining(ActionEvent actionEvent) {
        float result = calulateBMR() + 0;
        insertBMR(result);
    }

    /**
     * Calculates users BMR
     *
     * @return value of BMR
     */
    public float calulateBMR() {
        boolean violations = true;
        float bmr = 0;

        if (!height.getText().matches("[\\d]*[\\.]?[\\d]+")) {
            height.setPromptText("Miałeś podać liczbę (dodatnią) ");
            violations = false;
        }

        if (!age.getText().matches("[\\d]*[\\.]?[\\d]+")) {
            age.setPromptText("Miałeś podać liczbę (dodatnią)");
            violations = false;
        }
        if (!weight.getText().matches("[\\d]*[\\.]?[\\d]+")) {
            weight.setPromptText("Miałeś podać liczbę (dodatnią)");
            violations = false;
        }
        if (violations) {
            float wzrost1 = Float.parseFloat(height.getText());
            float wiek1 = Float.parseFloat(age.getText());
            float waga1 = Float.parseFloat(weight.getText());

            bmr = (float) (66 + (13.7 * waga1) + (5 * wzrost1) - (6.76 * wiek1));
        }
        return bmr;
    }

    /**
     * Inserts users BMR into database to BMR table.
     *
     * @param result value of BMR calculated {@link #calulateBMR()}
     */
    private void insertBMR(float result) {
        DecimalFormat d = new DecimalFormat("#");


        String sqlInsert = "INSERT INTO bmr"
                + "(bmr)"
                + "VALUES (?);";
        try {
            PreparedStatement pst = con.prepareStatement(sqlInsert);
            pst.setFloat(1, result);
            pst.executeUpdate();

            this.result.setText(String.valueOf(d.format(result)) + " kcal");
        } catch (SQLException ex) {
           throw new RuntimeException("Błąd podczas zapytania do bazy danych");
        }
    }


}
