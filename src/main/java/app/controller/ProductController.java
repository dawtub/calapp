package app.controller;

import app.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import app.ModelTable;

import javax.swing.*;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

public class ProductController extends TabsController implements Initializable {
    @FXML
    private TableView<ModelTable> table;
    @FXML
    private TableColumn<ModelTable, String> col_title;
    @FXML
    private TableColumn<ModelTable, String> col_calories;
    @FXML
    private TableColumn<ModelTable, String> col_proteins;
    @FXML
    private TableColumn<ModelTable, String> col_carbs;
    @FXML
    private TableColumn<ModelTable, String> col_fats;
    @FXML
    private javafx.scene.control.TextField title;
    @FXML
    private javafx.scene.control.TextField calories;
    @FXML
    private javafx.scene.control.TextField proteins;
    @FXML
    private javafx.scene.control.TextField carbs;
    @FXML
    private javafx.scene.control.TextField fats;

    private Connection con = Main.dbCon;

    private final ObservableList<ModelTable> productList = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        displayProducts();
    }

    /**
     * Adds product into database and table with specified name and macros.
     *
     * @param actionEvent
     */
    public void addProduct(ActionEvent actionEvent) {

        String query = "INSERT INTO produkty"
                + "(title, calories, proteins, carbs, fats)"
                + "VALUES (?,?,?,?,?);";

       try {
           PreparedStatement pst = con.prepareStatement(query);
           pst.setString(1, title.getText());
           pst.setString(2, calories.getText());
           pst.setString(3, proteins.getText());
           pst.setString(4, carbs.getText());
           pst.setString(5, fats.getText());
           pst.executeUpdate();
       }catch (SQLException e){
           throw new RuntimeException("Błąd podczas wykonywania zapytania");
       }

        JOptionPane.showMessageDialog(null,
                "Dodano produkt!",
                "Dodawanie",
                JOptionPane.PLAIN_MESSAGE);

        productList.clear();
        displayProducts();

        title.clear();
        calories.clear();
        proteins.clear();
        carbs.clear();
        fats.clear();
    }


    /**
     * Deletes product from table and database.
     *
     * @param actionEvent
     */
    public void deleteProduct(ActionEvent actionEvent) {

        String query = "DELETE FROM produkty WHERE title=?";

        try {
            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1, title.getText());
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Błąd podczas wykonywania zapytania");
        }

        JOptionPane.showMessageDialog(null,
                "Usunięto produkt!",
                "Usuwanie",
                JOptionPane.PLAIN_MESSAGE);

        productList.clear();
        displayProducts();
    }

    /**
     * Searches given product
     *
     * @param actionEvent
     */
    public void searchForProduct(ActionEvent actionEvent) {
        String title = "";
        float calories = 0;
        float proteins = 0;
        float carbs = 0;
        float fats = 0;

        String query = "SELECT * FROM produkty WHERE title='" + this.title.getText() + "';";

        try {
            ResultSet rs = con.createStatement().executeQuery(query);
            while (rs.next()) {
                title = rs.getString("title");
                calories = rs.getFloat("calories");
                proteins = rs.getFloat("proteins");
                carbs = rs.getFloat("carbs");
                fats = rs.getFloat("fats");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Błąd podczas wykonywania zapytania");
        }

        Object[] object = {"Produkt: " + title,
                "Kcal/100g: " + calories,
                "Bialko/100g: " + proteins,
                "Wegle/100g: " + carbs,
                "Tluszcze/100g: " + fats,
        };
        JOptionPane.showMessageDialog(null, object,
                "Wynik wyszukiwania",
                JOptionPane.PLAIN_MESSAGE);

        productList.clear();
        displayProducts();

    }

    /**
     * Shows products in table fetched from database
     */
    public void displayProducts() {
        try {
            ResultSet rs = con.createStatement().executeQuery("SELECT * FROM produkty;");
            while (rs.next()) {
                productList.add(new ModelTable(rs.getString("title"),
                        rs.getString("calories"),
                        rs.getString("proteins"),
                        rs.getString("fats"),
                        rs.getString("carbs")));
            }

        } catch (SQLException ex) {
            throw new RuntimeException("Błąd podczas wykonywania zapytania");
        }

        col_title.setCellValueFactory(new PropertyValueFactory<>("title"));
        col_calories.setCellValueFactory(new PropertyValueFactory<>("calories"));
        col_proteins.setCellValueFactory(new PropertyValueFactory<>("proteins"));
        col_carbs.setCellValueFactory(new PropertyValueFactory<>("carbs"));
        col_fats.setCellValueFactory(new PropertyValueFactory<>("fats"));

        table.setItems(productList);
    }
}
