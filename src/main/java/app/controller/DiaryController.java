package app.controller;

import java.sql.*;

import app.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;

import javax.swing.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DiaryController extends TabsController {

    // Dzienniczek.fxml section
    @FXML
    public TextField count;
    @FXML
    public TextField title;
    @FXML
    private PieChart chart;
    @FXML
    private ProgressBar caloriesProgress;
    @FXML
    private Label calories;

    private Connection con = Main.dbCon;

    private final ObservableList<PieChart.Data> pieChart = FXCollections.observableArrayList();

    /**
     * Shows actual system date.
     *
     * @return String with actual date
     */
    private String date() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * Calculates and adds calories into daily calories demand at database
     *
     * @param ilosc grams of product
     */
    private float calculateCalories(float ilosc) {
        float proteins = 0;
        float carbs = 0;
        float fats = 0;
        ResultSet rs;

        String query = "SELECT proteins, carbs, fats FROM produkty WHERE title='" + title.getText() + "';";

        try {
            rs = con.createStatement().executeQuery(query);
            while (rs.next()) {
                proteins = rs.getFloat("proteins");
                carbs = rs.getFloat("carbs");
                fats = rs.getFloat("fats");
            }
        } catch (SQLException e) {
            throw new RuntimeException("Błąd podczas wykonywania zapytania");
        }

        return (ilosc / 100) * ((4 * proteins) + (4 * carbs) + (9 * fats));
    }

    /**
     * Adds products to calories set
     *
     * @param actionEvent
     */
    public void addEatenProduct(ActionEvent actionEvent) {
        float calories = calculateCalories(Float.valueOf(count.getText()));
        String query = "INSERT INTO dzienniczek (data, calories) VALUES (?,?);";

        try {
            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1, date());
            pst.setFloat(2, calories);
            pst.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex);
        }
        JOptionPane.showMessageDialog(null,
                "Dodano do bilansu!",
                "Bilans zaaktualizowany",
                JOptionPane.PLAIN_MESSAGE);
    }

    /**
     * Calculates how much calories users ate.
     *
     * @return value of eaten calories
     */
    private double displayEatenMacro() {
        float makro = 0;
        String data = date();

        data = data.substring(0, 10);
        String query = "SELECT SUM(calories) AS zjedzone FROM dzienniczek WHERE data LIKE'" + data + " %';";

        try {
            ResultSet rs = con.createStatement().executeQuery(query);
            while (rs.next()) {
                makro = rs.getFloat("zjedzone");
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return makro;

    }

    /**
     * Creates PieChart of eaten and missing calories.
     */
    public void displayChart() {
        double bmr = 0;
        double eaten;
        double left;

        try {
            ResultSet rs = con.createStatement().executeQuery("SELECT bmr FROM bmr;");
            while (rs.next()) {
                bmr = rs.getFloat("bmr");
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        eaten = displayEatenMacro();
        left = bmr - eaten;

        pieChart.add(new PieChart.Data("Brakujące calories", left));
        pieChart.add(new PieChart.Data("Spożyte calories", eaten));

        chart.setTitle("Bilans kalorii");
        chart.setData(pieChart);

        calories.setText((int) eaten + " / " + (int) bmr + " kcal");

        caloriesProgress.setProgress(((100 * eaten) / bmr) / 100);
        caloriesProgress.setVisible(true);
    }


}
