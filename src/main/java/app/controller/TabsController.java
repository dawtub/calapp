package app.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

public class TabsController {

    /**
     * Creates stage reliable for Produkty tab.
     *
     * @param actionEvent
     * @throws IOException
     */
    public void selectProductView(ActionEvent actionEvent) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("/views/Produkty.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.getIcons().add(new Image("/icon.jpg"));
        window.setScene(tableViewScene);
        window.show();
    }

    /**
     * Creates stage reliable for BMR tab.
     *
     * @param actionEvent
     * @throws IOException
     */
    public void selectBmrView(ActionEvent actionEvent) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("/views/BMR.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.getIcons().add(new Image("/icon.jpg"));
        window.setScene(tableViewScene);
        window.show();
    }

    /**
     * Creates stage reliable for Wykresy tab.
     *
     * @param actionEvent
     * @throws IOException
     */
    public void selectChartView(ActionEvent actionEvent) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("/views/Dzienniczek.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.getIcons().add(new Image("/icon.jpg"));
        window.setScene(tableViewScene);
        window.show();
    }
}
