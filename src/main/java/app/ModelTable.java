package app;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ModelTable {
    private String title;
    private String calories;
    private String proteins;
    private String carbs;
    private String fats;
}
