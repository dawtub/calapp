-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 30 Maj 2018, 12:31
-- Wersja serwera: 10.1.32-MariaDB
-- Wersja PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `produkty`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `bmr`
--

CREATE TABLE `bmr` (
  `bmrNo` int(11) NOT NULL,
  `bmr` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `bmr`
--

INSERT INTO `bmr` (`bmrNo`, `bmr`) VALUES
(25, 1961.14),
(26, 2256.14),
(27, 1961.14);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dzienniczek`
--

CREATE TABLE `dzienniczek` (
  `data` datetime NOT NULL,
  `calories` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `dzienniczek`
--

INSERT INTO `dzienniczek` (`data`, `calories`) VALUES
('2018-05-29 00:00:00', 0),
('2018-05-29 23:01:06', 98),
('2018-05-29 23:02:45', 122),
('2018-05-30 12:18:29', 146);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkty`
--

CREATE TABLE `produkty` (
  `idProd` int(11) NOT NULL,
  `title` varchar(30) COLLATE utf8_polish_ci NOT NULL,
  `calories` float NOT NULL,
  `proteins` float NOT NULL,
  `carbs` float NOT NULL,
  `fats` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `produkty`
--

INSERT INTO `produkty` (`idProd`, `title`, `calories`, `proteins`, `carbs`, `fats`) VALUES
(1, 'proteins', 407.5, 75, 10, 7.5),
(2, 'kurczak', 100, 21, 0, 1.5),
(5, 'banan', 77, 1.1, 19.2, 0),
(6, 'ryz', 349, 6.7, 78.9, 0),
(17, 'test', 0, 0, 0, 0),
(18, 'tluszcz', 100, 0, 0, 100),
(19, 'carbs', 100, 0, 100, 0),
(20, 'cola-cola', 42, 0, 10.4, 0),
(21, 'sok pomaranczowy', 44, 0.7, 10, 0.1),
(22, 'whisky', 220, 0, 0, 0),
(23, 'jablko', 52, 0.26, 13.8, 0.17),
(24, 'Snickers', 497, 9.7, 52.6, 28.9),
(25, 'brokuly', 25, 2.9, 5.7, 0.3),
(26, 'cebula', 31, 1.2, 7.3, 0.3),
(27, 'ziemniaki', 75, 1.7, 19, 0.2),
(28, 'jajko kurze', 143, 12.6, 0.7, 9.5),
(29, 'losos', 206, 21.1, 0, 14.5),
(30, 'ser mozarella', 251, 11.7, 23.8, 12.1),
(31, 'twarog chudy', 95, 20, 3.2, 0.4);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `bmr`
--
ALTER TABLE `bmr`
  ADD PRIMARY KEY (`bmrNo`);

--
-- Indeksy dla tabeli `dzienniczek`
--
ALTER TABLE `dzienniczek`
  ADD PRIMARY KEY (`data`);

--
-- Indeksy dla tabeli `produkty`
--
ALTER TABLE `produkty`
  ADD PRIMARY KEY (`idProd`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `bmr`
--
ALTER TABLE `bmr`
  MODIFY `bmrNo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT dla tabeli `produkty`
--
ALTER TABLE `produkty`
  MODIFY `idProd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
